# 说明
电子文档
## 路径
- Meshing进阶攻略/
- CFD从入门到入土.pdf
- Ansys_Fluent_Theory_Guide.pdf      
    - **Ansys 2022R1 fluent theory guide**
- Ansys_Fluent_User_Guide.zip.001     
    - **Ansys 2022R1 fluent user guide 压缩包1**
- Ansys_Fluent_User_Guide.zip.002     
    - **Ansys 2022R1 fluent user guide 压缩包2 将两个压缩包放到一个路径下，解压第一个压缩包即可**