# Change Log

## 2022-6-17 16:04:41

### Add

- Bookmark

## 2022-6-17 15:27:02
### Add
- Assembly Meshing
- Inflation of Meshing Methods
## 2022-6-15 21:17:10
### Add
- Introduction to boundary layers
- Wall functions and near-wall treatment

## 2022-6-9 16:06:11
### Add

- 全局网格控制/Defaults
- 全局网格控制/Sizing
- 全局网格控制/Inflation
- 全局网格控制/Advanced Inflation Options
- 全局网格控制/Quality
- Mesh方法/Patch Conforming
- Mesh方法/Patch Independent
- Mesh方法/Sweep
- Mesh方法/Multizone
- Mesh方法/Cartesian
- Mesh方法/Automatic
- Mesh方法/Surface Methods

